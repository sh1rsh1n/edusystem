package com.ems.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyController
{
    @RequestMapping("/")
    public String showFirstView()
    {
        return "first-view";
    }

    @RequestMapping("/askStudentsInfo")
    public String askStudentsInfo()
    {
        return "ask-student-info";
    }

    @RequestMapping("/showStudentsInfo")
    public String showStudentsInfo()
    {
        return "show-students-info";
    }
}
