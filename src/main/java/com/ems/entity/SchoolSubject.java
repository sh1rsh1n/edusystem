package com.ems.entity;

import javax.persistence.*;

@Entity
@Table(name = "subject")
public class SchoolSubject
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "students_group")
    private StudentsGroup studentsGroup;

    public SchoolSubject()
    {
    }

    public SchoolSubject(String name, StudentsGroup studentsGroup)
    {
        this.name = name;
        this.studentsGroup = studentsGroup;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public StudentsGroup getStudentsGroup()
    {
        return studentsGroup;
    }

    public void setStudentsGroup(StudentsGroup studentsGroup)
    {
        this.studentsGroup = studentsGroup;
    }

    @Override
    public String toString()
    {
        return "SchoolSubject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", studentsGroup=" + studentsGroup +
                '}';
    }
}
