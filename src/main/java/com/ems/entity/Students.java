package com.ems.entity;

import com.ems.security.Users;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "students")
public class Students
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "middlename")
    private String middleName;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "address")
    private String address;

    @Column(name = "email")
    private String email;

    @Column(name = "students_group_id")
    private StudentsGroup studentsGroup;

    @Column(name = "phone")
    private String phoneNumber;

    @Column(name = "username")
    private Users users;

    public Students()
    {
    }

    public Students(String firstName, String lastName, String middleName, Date birthDate, String address, String email, StudentsGroup studentsGroup, String phoneNumber, Users users)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.birthday = birthDate;
        this.address = address;
        this.email = email;
        this.studentsGroup = studentsGroup;
        this.phoneNumber = phoneNumber;
        this.users = users;
    }

    public Users getUsers()
    {
        return users;
    }

    public void setUsers(Users users)
    {
        this.users = users;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getMiddleName()
    {
        return middleName;
    }

    public void setMiddleName(String middleName)
    {
        this.middleName = middleName;
    }

    public Date getBirthday()
    {
        return birthday;
    }

    public void setBirthday(Date birthday)
    {
        this.birthday = birthday;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public StudentsGroup getStudentsGroup()
    {
        return studentsGroup;
    }

    public void setStudentsGroup(StudentsGroup studentsGroup)
    {
        this.studentsGroup = studentsGroup;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString()
    {
        return "Students{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthDate=" + birthday +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", studentsGroup=" + studentsGroup +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
