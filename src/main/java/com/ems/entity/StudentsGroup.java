package com.ems.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "students_group")
public class StudentsGroup
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "number")
    private int number;

    private List<Students> studentsList;

    public StudentsGroup()
    {
    }

    public StudentsGroup(int number, List<Students> studentsList)
    {
        this.number = number;
        this.studentsList = studentsList;
    }

    public List<Students> getStudentsList()
    {
        return studentsList;
    }

    public void setStudentsList(List<Students> studentsList)
    {
        this.studentsList = studentsList;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getNumber()
    {
        return number;
    }

    public void setNumber(int number)
    {
        this.number = number;
    }

    @Override
    public String toString()
    {
        return "StudentsGroup{" +
                "id=" + id +
                ", number=" + number +
                ", studentsList=" + studentsList +
                '}';
    }
}
