package com.ems.security;

import com.ems.entity.SchoolSubject;
import com.ems.entity.Students;
import com.ems.entity.StudentsGroup;
import com.ems.entity.Teachers;

public interface AdminPanel
{
    public Teachers addTeacher();

    public void updateTeacher();

    public Students addStudent();

    public void updateStudents();

    public SchoolSubject addSubject();

    public void updateSubject();

    public StudentsGroup addGroup();

    public void updateGroup();

    public void setRole();
}
