package com.ems.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role
{
    @Column(name = "username")
    private String userName;

    @Column(name = "access")
    private String access;

    public Role()
    {
    }

    public Role(String userName, String access)
    {
        this.userName = userName;
        this.access = access;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getAccess()
    {
        return access;
    }

    public void setAccess(String access)
    {
        this.access = access;
    }
}
