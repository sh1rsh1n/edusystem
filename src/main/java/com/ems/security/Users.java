package com.ems.security;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class Users
{
    @Column(name = "username")
    private String userName;

    @Column(name = "password")
    private String password;

    private Role role;

    public Users()
    {
    }

    public Users(String userName, String password)
    {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }


}
